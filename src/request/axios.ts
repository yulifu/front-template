import axios, { AxiosInstance, AxiosRequestConfig, AxiosResponse } from 'axios'
import i18n from '@/i18n'

interface Response<T = any> {
  success: boolean
  data: T
  code: number
  msg?: string
  [key: string]: any
}

export class ResponseError<T = any> extends Error {
  msg: string
  data: T | undefined
  code: number | undefined

  constructor (msg?: string, code?: number, data?: T) {
    const _message = msg ?? i18n.global.t('error.network')
    super(_message)
    this.name = 'ResponseError'

    this.msg = _message
    this.data = data
    this.code = code
  }
}

interface Instance extends AxiosInstance {
  <T, R = Response<T>>(config: AxiosRequestConfig): Promise<R>
  <T, R = Response<T>>(url: string, config?: AxiosRequestConfig): Promise<R>
  request: <T, R = Response<T>>(config: AxiosRequestConfig) => Promise<R>
  get: <T, R = Response<T>>(url: string, config?: AxiosRequestConfig) => Promise<R>
  delete: <T, R = Response<T>>(url: string, config?: AxiosRequestConfig) => Promise<R>
  head: <T, R = Response<T>>(url: string, config?: AxiosRequestConfig) => Promise<R>
  post: <T, R = Response<T>>(url: string, data?: any, config?: AxiosRequestConfig) => Promise<R>
  put: <T, R = Response<T>>(url: string, data?: any, config?: AxiosRequestConfig) => Promise<R>
  patch: <T, R = Response<T>>(url: string, data?: any, config?: AxiosRequestConfig) => Promise<R>
}

const instance: Instance = axios.create({
  baseURL: import.meta.env.VITE_REQUEST_DOMAIN
})

instance.interceptors.request.use((config: AxiosRequestConfig) => {
  config.params = Object.assign({}, {})
  
  return config
})

instance.interceptors.response.use(async (res: AxiosResponse) => {
  // const { data: { data, code, result, msg } } = res
  return Promise.resolve(res)
}, async () => {
  return Promise.reject(new ResponseError(i18n.global.t('error.network')))
})

export default instance
