import Home from '@/pages/home/index.vue'

const routes = [
    { path: '/', component: Home }
]

export default routes